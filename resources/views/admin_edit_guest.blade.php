@extends('admin_layout')
@section('content')
<div class="page-container"> 
	<div class="left-content">
		<div class="mother-grid-inner">
<div class="grid-form1">
  	       <h3>Form Element</h3>
  	         <div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<form class="form-horizontal" action="{{route('update_guest',$editguest->id)}}" method="POST" 
								enctype="multipart/form-data">
								@csrf
  		    					@method('PUT')
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">name</label>
									<div class="col-sm-8">
										<input type="text" name="name" class="form-control1" id="focusedinput" value="{{$editguest->name}}">
									</div>
								</div>
								<div class="form-group">
									<label for="disabledinput" class="col-sm-2 control-label">surname</label>
									<div class="col-sm-8">
										<input name="surname" type="text" class="form-control1" id="disabledinput" value="{{$editguest->surname}}">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">description</label>
									<div class="col-sm-8">
										<input type="text" name="description" class="form-control1" id="focusedinput" value="{{$editguest->description}}">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">key</label>
									<div class="col-sm-8">
										<input type="text" name="key" class="form-control1" id="focusedinput" value="{{$editguest->key}}">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">curent image</label>
									<img src="{{asset('images/'.$editguest->image)}}"  class="img-fluid" alt=""/>
								</div>

								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label"> new image</label>
									<div class="col-sm-8">
										<input type="file" name="image"  accept="png, jpg, jpeg, svg, gif">
									</div>
								</div>
								<button type="submit" class="btn-primary btn">Submit</button>
							</form>
						</div>
					</div>
					@endsection
