<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BlueBerryBirthday</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Free HTML5 Template by FREEHTML5.CO" />
	<meta name="keywords" content="free html5, free template, free bootstrap, html5, css3, mobile first, responsive" />
	<meta name="author" content="FREEHTML5.CO" />

  <!-- 
	//////////////////////////////////////////////////////

	FREE HTML5 TEMPLATE 
	DESIGNED & DEVELOPED by FREEHTML5.CO
		
	Website: 		http://freehtml5.co/
	Email: 			info@freehtml5.co
	Twitter: 		http://twitter.com/fh5co
	Facebook: 		https://www.facebook.com/fh5co

	//////////////////////////////////////////////////////
	 -->

  	<!-- Facebook and Twitter integration -->
	<meta property="og:title" content=""/>
	<meta property="og:image" content=""/>
	<meta property="og:url" content=""/>
	<meta property="og:site_name" content=""/>
	<meta property="og:description" content=""/>
	<meta name="twitter:title" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:url" content="" />
	<meta name="twitter:card" content="" />

	<link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href={{ asset('css/inner/animate.css') }}>
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href={{ asset('css/inner/icomoon.css') }}>
	<!-- Bootstrap  -->
	<link rel="stylesheet" href={{ asset('css/inner/bootstrap.css') }}>

	<!-- Magnific Popup -->
	<link rel="stylesheet" href={{ asset('css/inner/magnific-popup.css') }}>

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href={{ asset('css/inner/owl.carousel.min.css') }}>
	<link rel="stylesheet" href={{ asset('css/inner/owl.theme.default.min.css') }}>

	<!-- Theme style  -->
	<link rel="stylesheet" href={{ asset('css/inner/style.css') }}>

	<!-- Modernizr JS -->
	<script src={{ asset('js/inner/modernizr-2.6.2.min.js') }}></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src={{ asset('js/respond.min.js') }}></script>
	<![endif]-->

	</head>
	<body>
	<div class="fh5co-loader"></div>
	
	<div id="page">

	<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url({{asset('images/background123.jpg') }});">
		<div class="overlay"></div>
		<div class="fh5co-container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>Birthday story</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<div id="fh5co-couple" class="fh5co-section-gray">
		<div class="container">
			<div class="row">

				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h2>Alloha {{$guest->name}}</h2>
					<h3>May 27th, 2018 Sofia, Bulgaria</h3>
					<p>A man was born !</p>
				</div>
			</div>
			<div class="couple-wrap animate-box">
				<div class="couple-half">
					<div class="groom">
						<img src={{asset('images/stef_profile.jpg')}} alt="groom" class="img-responsive">
					</div>
					<div class="desc-groom">
						<h2>Stefan Gadjev</h2>
						<p>An emotional kid with tall legs and crispy eyes. His little world was build around his family and friends lifes.</p>
					</div>
				</div>
				<p class="heart text-center"><i class="icon-heart2"></i></p>
				<div class="couple-half">
					<div class="bride">
						<img src="{{asset('images/'.$guest->image)}}" alt="groom" class="img-responsive">
					</div>
					<div class="desc-bride">
						<h2>{{$guest->name}} {{$guest->surname}}</h2>
						<p>{{$guest->description}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-couple-story">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					
					<h2>My Story</h2>
					<span>with you </span>
					<p>(:Far far away, our friendship once started, full with drama and lovely excitements:)</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-md-offset-0">
					<ul class="timeline animate-box">
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url({{asset('images/me_little.jpg')}})"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">My First day</h3>
									<span class="date">(May 27, 1996)</span>
								</div>
								<div class="timeline-body">
									<p>I was born alone from the darkness inside . My life was empty and not full with excite.
									but then i grow up and find my friends, that modulate my soul as a part of theirs.</p>
								</div>
							</div>
						</li>
						<li class="timeline-inverted animate-box">
							<div class="timeline-badge" style="background-image:url({{asset('images/stef_profile.jpg')}})"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">My last day </h3>
									<span class="date">(till now :| )</span>
								</div>
								<div class="timeline-body">
									<p>Years past and i grew strong ! thanks for the help, care and support.
									My character is partly yours. But still i own it in a different corridor. </p>
								</div>
							</div>
						</li>
						<li class="animate-box">
							<div class="timeline-badge" style="background-image:url({{asset('images/'.$guest->image)}});"></div>
							<div class="timeline-panel">
								<div class="timeline-heading">
									<h3 class="timeline-title">Our Friendship</h3>
									<span class="date">(through my eyes)</span>
								</div>
								<div class="timeline-body">
									<p>Friends are flowers in the garder of life. Thank you for planting me with water and sunrise ! I see the difference in the the world from before. The friendship is the gift i appreciate the most. </p>
								</div>
							</div>
						</li>
			    	</ul>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-gallery">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					
					<h2> Our Memories</h2>
					<span>the best that i got tho</span>
				</div>
			</div>
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-gallery-list">
						


                         @foreach($guest->photoGallery as $gallery)
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url({{asset('images/'.$gallery->guest_image)}});"> 
							<a href="#" class="color-5">
								<div class="case-studies-summary">
									<h2>{{$gallery->title}}</h2>
								</div>
							</a>
						</li>
						@endforeach
					</ul>		
				</div>
			</div>
		</div>
	</div>

	<div class="fh5co-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-push-6 animate-box">
				</div>
				<div class="col-md-5 col-md-pull-5 animate-box">
					
					<div class="fh5co-contact-info">
						<h3>The Place And the Time of The Crime</h3>
						<ul>
							<li>Ейл Хаус, 20:00h<br> ул. „Христо Белчев“ 42, 1000 Център, София</li>
						</ul>
						<h3>My Number:</h3>
						<ul>
							<li>+ 088 63 92 058</a></li>
						</ul>
					</div>

				</div>
			</div>
			
		</div>
	</div>

	<div style="width: 100%">
				<iframe width="100%" height="600" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1874.354040018689!2d23.321696807167044!3d42.689009180491155!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40aa85129abee2b3%3A0x277da7e99a45e27d!2sAle+House!5e0!3m2!1sen!2sbg!4v1526664710440" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/create-google-map/">Google map generator</a>
				</iframe>
			</div>


	<!-- jQuery -->
	<script src={{ asset('js/inner/jquery.min.js') }}></script>
	<!-- jQuery Easing -->
	<script src={{ asset('js/inner/jquery.easing.1.3.js') }}></script>
	<!-- Bootstrap -->
	<script src={{ asset('js/inner/bootstrap.min.js') }}></script>
	<!-- Waypoints -->
	<script src={{ asset('js/inner/jquery.waypoints.min.js') }}></script>
	<!-- Carousel -->
	<script src={{ asset('js/inner/owl.carousel.min.js') }}></script>
	<!-- countTo -->
	<script src={{ asset('js/inner/jquery.countTo.js') }}></script>

	<!-- Stellar -->
	<script src={{ asset('js/inner/jquery.stellar.min.js') }}></script>
	<!-- Magnific Popup -->
	<script src={{ asset('js/inner/jquery.magnific-popup.min.js') }}></script>
	<script src={{ asset('js/inner/magnific-popup-options.js') }}></script>

	<!-- Google Map -->
	<script src={{ asset('https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false') }}></script>
	<script src={{ asset('js/inner/google_map.js') }}></script>


	<!-- Main -->
	<script src={{ asset('js/main.js') }}></script>

	</body>
</html>

