@extends('admin_layout')
@section('content')

   <div class="page-container">
   <!--/content-inner-->
<div class="left-content">
	   <div class="mother-grid-inner">
              <!--header start here-->
							
				     <div class="clearfix"> </div>	
				</div>
		<!--grid-->
<div class="grid-form">
 	<form action="{{route('product_create')}}" method="POST" enctype="multipart/form-data" >
 		@csrf
 		<div class="grid-form1">
	 		<h2 id="forms-example" class="">Item Upload</h2>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">Item name</label>
				    <input type="text" class="form-control" name="name" placeholder="Item name">
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputPassword1">price</label>
				    <input type="number" class="form-control" name="price" placeholder="price">
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">description</label>
				    <input type="text" name="description" class="form-control" placeholder="description">
			  	</div>
			  	<div class="form-group">
				    <label for="">category</label>
                    <div class="clearfix"></div>
                    <select  class="choice" name="category_id"> 
		                @foreach($categories as $category)
		                    	<option value="{{$category->id}}">{{$category->name}}</option>
		                @endforeach
                    </select>
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">quantity</label>
				    <div class="clearfix"></div>
				    <input type="number" name="quantity" min="1" max="100">
			  	</div>
			  	<div class="form-group">
				    <label for="exampleInputEmail1">video</label>
				    <input type="text" name="video" class="form-control" placeholder="url">
			  	</div>
			  	<div class="form-group">
			        <label for="exampleInputFile">File input</label>
			        <input type="file" name="image"  accept="png, jpg, jpeg, svg, gif">
			 	</div>
			  	<button type="submit" class="btn-primary btn">Submit</button>
		</div>   
	</form>
</div>
<div class="grid-form">
 	<form action="{{ route('categoy_create') }}" method="POST">

 		<div class="grid-form1">
 			@csrf
 			<h2 id="forms-example" class="">Category Upload</h2>
			  	<div class="form-group">
			    	<label for="exampleInputEmail1">category</label>
			    	<input type="text" class="form-control" name="name"  placeholder="category">
		  		</div>
		  	<button type="submit" class="btn-primary btn">Submit</button>
		  	</div>
	</form>
</div>
@endsection
