@extends('admin_layout')
@section('content')
<div class="page-container"> 
	<div class="left-content">
		<div class="mother-grid-inner">
			<div class="agile-grids">	
				<table class="table table-dark">
					  <thead>
					    <tr>
					      <th scope="col">Name</th>
					      <th scope="col">Surname</th>
					      <th scope="col">Key</th>
					      <th scope="col">Edit</th>
					       <th scope="col">Delete</th>

					    </tr>
					  </thead>
			
					  <tbody>
					  @foreach($guests as $guest)
					    <tr>
					      <td>{{$guest->name}}</td>
					      <td>{{$guest->surname}}</td>
					      <td>{{$guest->key}}</td>
					      <td><a href="{{route('edit_guest',$guest->id)}}" class="btn-primary btn" >edit</td>
					      <td><a href="#" class="btn-primary btn"
                                 onclick="event.preventDefault();
                                 document.getElementById('delete_product_{{$guest->id}}').submit();">
                                 {{ __('Delete') }}
                                </a>
                                <form id="delete_product_{{$guest->id}}" action="{{route('delete_guest',$guest->id)}}" method="POST" style="display: none;">
                                        @csrf
                                        @method('DELETE')
                                </form></td>
					    </tr>
					   @endforeach 
					  </tbody>	  
				</table>
		
			</div>
		</div>
	</div>
</div>
@endsection