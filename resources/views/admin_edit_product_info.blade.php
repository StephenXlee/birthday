@extends('admin_layout')
@section('content')
<div class="page-container"> 
	<div class="left-content">
		<div class="mother-grid-inner">
<div class="grid-form1">
  	       <h3>Form Element</h3>
  	         <div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<form class="form-horizontal" action="{{Route('guest_upload')}}" method="POST" 
								enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">First name</label>
									<div class="col-sm-8">
										<input type="text" name="name" class="form-control1" id="focusedinput" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Surname</label>
									<div class="col-sm-8">
										<input type="text" name="surname" class="form-control1" id="focusedinput" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">description</label>
									<div class="col-sm-8">
										<input type="text" name="description" class="form-control1" id="focusedinput" value="">
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label"> Profile Image</label>
									<div class="col-sm-8">
										<input type="file" name="image"  accept="png, jpg, jpeg, svg, gif">
									</div>
								</div>
								
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">Invite Key</label>
									<div class="col-sm-8">
										<input type="text" name="key" class="form-control1" id="focusedinput" value="">
									</div>
								</div>
								
								<button type="submit" class="btn-primary btn">Submit</button>
								{{-- <div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label"> new image</label>
									<div class="col-sm-8">
										<input type="file" name="image"  accept="png, jpg, jpeg, svg, gif">
									</div>
								</div>
								<button type="submit" class="btn-primary btn">Submit</button> --}}
							</form>
						</div>
					</div>
					@endsection
