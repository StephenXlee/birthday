@extends('admin_layout')
@section('content')
<div class="page-container"> 
	<div class="left-content">
		<div class="mother-grid-inner">
			<div class="agile-grids">	
				<table class="table table-dark">
					  <thead>
					    <tr>
					      <th scope="col">guest</th>
					      <th scope="col">title</th>
					      <th scope="col">image name</th>
					       <th scope="col">Delete</th>
{{-- allphotos --}}
					    </tr>
					  </thead>
			
					  <tbody>
					  @foreach($allphotos as $photo)
					    <tr>
					      <td>{{$photo->guest->name}}</td>
					      <td>{{$photo->title}}</td>
					      <td>{{$photo->guest_image}}</td>
					      <td><a href="#" class="btn-primary btn"
                                 onclick="event.preventDefault();
                                 document.getElementById('delete_product_{{$photo->id}}').submit();">
                                 {{ __('Delete') }}
                                </a>
                                <form id="delete_product_{{$photo->id}}" action="{{route('delete_guest_photo',$photo->id)}}" method="POST" style="display: none;">
                                        @csrf
                                        @method('DELETE')
                                </form></td>
					  @endforeach
					  </tbody>	  
				</table>
		
			</div>
		</div>
	</div>
</div>
@endsection