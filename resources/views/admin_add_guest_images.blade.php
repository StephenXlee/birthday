@extends('admin_layout')
@section('content')
<div class="page-container"> 
	<div class="left-content">
		<div class="mother-grid-inner">
<div class="grid-form1">
  	       <h3>Form Element</h3>
  	         <div class="tab-content">
						<div class="tab-pane active" id="horizontal-form">
							<form class="form-horizontal" action="{{Route('guest_image_upload')}}" method="POST" 
								enctype="multipart/form-data">
								@csrf
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">guest</label>
									<div class="col-sm-8">
										<select  class="choice" name="guest_id"> 
		       								 @foreach($allguest as $guest)
		                    					<option value="{{$guest->id}}">{{$guest->name}}</option>
		               						@endforeach
                    					</select>
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label">title</label>
									<div class="col-sm-8">
										<input type="text" name="title" class="form-control1" id="focusedinput" >
									</div>
								</div>
								<div class="form-group">
									<label for="focusedinput" class="col-sm-2 control-label"> add image Image</label>
									<div class="col-sm-8">
										<input type="file" name="image"  accept="png, jpg, jpeg, svg, gif">
									</div>
								</div>
								
								<button type="submit" class="btn-primary btn">Submit</button>














