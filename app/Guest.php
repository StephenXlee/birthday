<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    public function photoGallery()
    {
      return $this->hasMany('App\PhotoGallery');
    }
}
