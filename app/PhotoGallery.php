<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoGallery extends Model
{
    public function guest()
    {
      return $this->belongsTo('App\Guest');
    }
}
