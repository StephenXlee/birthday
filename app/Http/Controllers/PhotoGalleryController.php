<?php

namespace App\Http\Controllers;

use App\PhotoGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoGalleryController extends Controller
{
    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('stefkoo')) . '.' . $image->getClientOriginalExtension();
    }

    public function store(Request $request)
    {
        if($request->hasFile('image') && $request->image->isValid()) {
        $image = $request->image;
        $image_name = $this->hashImageName($image);
        Storage::disk('images')->putFileAs('', $image, $image_name);

        $guest_id = $request->input('guest_id');
        $title = $request->input('title');
        
        $newphotoguest= new PhotoGallery;
        $newphotoguest->guest_id=$guest_id;
        $newphotoguest->title=$title;
        $newphotoguest->guest_image=$image_name;
        $newphotoguest->save();

        return redirect()->back();
    }
     return 'eba si maikata neshto';
    }

    public function show()
    {
        $allphotos=PhotoGallery::all();
        return view('admin_view_guest_images',compact('allphotos'));      
    }

     public function delete($id)
    {
        $getphoto=PhotoGallery::find($id);
        PhotoGallery::find($id)->delete();
        Storage::disk('images')->delete($getphoto->image);  
        return redirect()->back();    
    }
}

