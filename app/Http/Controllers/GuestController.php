<?php

namespace App\Http\Controllers;

use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class GuestController extends Controller
{
    protected function startup()
    {
        return view('loggin');
   
    }

    public function About(Request $request, $key)
    {
        $guest=Guest::where('key' ,$key)->first();
         return view('about', compact('guest'));
   
    }

    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('stefkoo')) . '.' . $image->getClientOriginalExtension();
    }

    public function GuestUpload(Request $request)
    {
        if($request->hasFile('image') && $request->image->isValid()) {
        $image = $request->image;
        $image_name = $this->hashImageName($image);
        Storage::disk('images')->putFileAs('', $image, $image_name);

        $name = $request->input('name');
        $surname = $request->input('surname');
        $description= $request->input('description');
        $key = $request->input('key');
        


        $newguest= new Guest;
        $newguest->name=$name;
        $newguest->surname=$surname;
        $newguest->description=$description;
        $newguest->image=$image_name;
        $newguest->key=$key;
        $newguest->save();

        return redirect()->route('all_guests');
    }
     return "da i bua";
    }

}