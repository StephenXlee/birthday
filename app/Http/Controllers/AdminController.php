<?php

namespace App\Http\Controllers;
use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use Session;

class AdminController extends Controller
{
    private function hashImageName($image)
    {
        return md5($image->getClientOriginalName() . uniqid('stefkoo')) . '.' . $image->getClientOriginalExtension();
    }

    public function Index()
    {
        return view('admin_edit_product_info');
    }

    public function AllGuests()
    {
    	$guests= Guest::all(); 
        return view('admin_all_products', compact('guests'));
    }

    public function PhotoGuests()
    {
        return view('admin_edit_product_info');
    }

     public function TestKey(Request $request)
    {
        try{    
                $key=$request->input('key');
                $guest=Guest::where('key', '=' , $key)->firstOrFail();
                    if($guest->key == $key)
                    {
                        return redirect()->route('about',$guest->key);
                    }
            }catch(\Exception $e)
            {
            return Redirect()->route('LoginPage');
            }
    }  

    public function DeleteGuest($id)
    {
        $image_delete=Guest::find($id);
        Guest::find($id)->delete();
        Storage::disk('images')->delete($image_delete->image);

        return Redirect()->back();
    }

    public function EdtGuest($id)
    {
       $editguest=Guest::find($id);
       return view('admin_edit_guest',compact('editguest'));
    }  

    public function UpdateGuest(Request $request,$id)
    {
       
        $updateguest=Guest::find($id);
        $name = $request->input('name');
        $surname = $request->input('surname');
        $description = $request->input('description');
        $key= $request->input('key');

         if($request->hasFile('image') && $request->image->isValid()) {
        $image = $request->image;
        $image_name = $this->hashImageName($image);
        Storage::disk('images')->putFileAs('', $image, $image_name);
        $imagedelete=$updateguest->image;
        Storage::disk('images')->delete($imagedelete);
        $updateguest->image=$image_name;
        }
                 
        $updateguest->name=$name;
        $updateguest->surname=$surname;
        $updateguest->description=$description;
        $updateguest->key=$key;
        
        $updateguest->save();
        return redirect()->route('all_guests');
    }  

       public function AddGuestImage()
    {
       $allguest=Guest::all();
        return view('admin_add_guest_images', compact('allguest'));
    } 

     public function login()
    {
       return view('login');
    } 
}


