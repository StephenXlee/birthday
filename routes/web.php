<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/' , 'GuestController@startup')->name('LoginPage');
Route::get('/about/{key}' , 'GuestController@About')->name('about');
Route::post('/trykey', 'AdminController@TestKey')->name('try_key');
Route::get('/login', 'AdminController@login')->name('login');

Route::group(['middleware' => 'admin'], function(){

Route::get('/admin' , 'AdminController@Index')->name('admin');
Route::Post('/admin/GuestUpload', 'GuestController@GuestUpload')->name('guest_upload');

Route::get('/admin/all_guests', 'AdminController@AllGuests')->name('all_guests');

Route::get('/admin/add_photos', 'AdminController@PhotoGuests')->name('photos_guests');

Route::get('/admin/guest/edit/{id}','AdminController@EdtGuest')->name('edit_guest');
Route::put('/admin/guest/update/{id}','AdminController@UpdateGuest')->name('update_guest');
Route::delete('/admin/guest/delete/{id}','AdminController@DeleteGuest')->name('delete_guest');

Route::get('/admin/guest/addImage','AdminController@AddGuestImage')->name('add_guest_image');
Route::post('/admin/guest/uploadImage','PhotoGalleryController@store')->name('guest_image_upload');

Route::get('/admin/view_guest_image','PhotoGalleryController@show')->name('view_guest_image');
Route::delete('/admin/delete_guest_image/{id}','PhotoGalleryController@delete')->name('delete_guest_photo');

});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
