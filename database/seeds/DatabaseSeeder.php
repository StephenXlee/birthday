<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'blueberrypie',
            'email' => 'stefangadjev@mail.bg',
            'password' => bcrypt('ilovevanilla12'),
            'admin' => '1',
        ]);
    }
}
